Ext.Loader.setConfig({
    enabled: true,
    disableCaching: false,
    paths: {
        'Ext.ux.exporter': 'exporter'
    }
});

Ext.require([
    "Ext.panel.Panel",
    "Ext.grid.*",
    "Ext.data.*",
    "Ext.window.*",
    "Ext.container.Viewport",
    "Ext.form.field.*",
    'Ext.ux.grid.JsonExport'
]);

Ext.onReady(function () {
    // View Port

    // Ext.create('Ext.container.Viewport', {
    //     layout: 'border',
    //     items: [{
    //         region: 'north',
    //         html: '<h1 class="x-panel-header">Page Title Goes Here</h1>',
    //         border: true,
    //         margin: '0 0 5 0'
    //     }, {
    //         region: 'west',
    //         collapsible: true,
    //         title: 'Navigation',
    //         width: 150
    //         // could use a TreePanel or AccordionLayout for navigational items
    //     }, {
    //         region: 'south',
    //         title: 'South Panel',
    //         collapsible: true,
    //         html: 'Information goes here',
    //         split: true,
    //         height: 100,
    //         minHeight: 100
    //     }, {
    //         region: 'east',
    //         title: 'East Panel',
    //         collapsible: true,
    //         split: true,
    //         width: 150
    //     }, {
    //         region: 'center',
    //         xtype: 'tabpanel', // TabPanel itself has no title
    //         activeTab: 0,      // First tab active by default
    //         items: {
    //             title: 'Default Tab',
    //             html: 'The first tab\'s content. Others may be added dynamically'
    //         }
    //     }]
    // });

    //button with container

    Ext.create('Ext.Button', {
        text    : 'Add Employee',
        margin: 20,
        renderTo: Ext.getBody(),
        handler : function() {
            // this button will spit out a different number every time you click it.
            // so firstly we must check if that number is already set:
            Ext.create('Ext.window.Window', {
                layout: 'fit',
                items: [
                    Ext.create('Ext.form.Panel', {
                        title: 'Create Eployee',
                        renderTo: Ext.getBody(),
                        bodyPadding: 5,
                        width: 350,
                    
                        // Any configuration items here will be automatically passed along to
                        // the Ext.form.Basic instance when it gets created.
                    
                        // The form will submit an AJAX request to this URL when submitted
                        items: [
                            Ext.create('Ext.form.field.Text', {
                                fieldLabel: 'Name',
                                name: 'name'
                            }),
                            Ext.create('Ext.form.field.Text', {
                                fieldLabel: 'Email',
                                name: 'email'
                            }),
                            Ext.create('Ext.form.field.Text', {
                                fieldLabel: 'Phone',
                                name: 'phone'
                            }),
                            Ext.create('Ext.form.field.Text', {
                                fieldLabel: 'Gender',
                                name: 'gender'
                            }),
                            Ext.create('Ext.form.field.Number', {
                                fieldLabel: 'Age',
                                name: 'age'
                            }),
                        ],
                    
                        buttons: [{
                            text: 'Submit',
                            handler: function() {
                                // The getForm() method returns the Ext.form.Basic instance:
                                var form = this.up('form').getForm();
                                var employeeName = form.findField("name").getValue();
                                var employeePhone = form.findField("phone").getValue();
                                var employeeEmail = form.findField("email").getValue();
                                var employeeAge = form.findField("age").getValue();
                                var employeeGender = form.findField("gender").getValue();
                                if (form.isValid()) {
                                    // Submit the Ajax request and handle the response
                                    form.submit({
                                        url: 'http://localhost:3000/api/add_employee',
                                        method: "POST",
                                        params: {
                                            name: employeeName,
                                            email: employeeEmail, 
                                            phone: employeePhone,
                                            age: employeeAge,
                                            gender: employeeGender
                                        },
                                        success: function(form, action) {
                                            Ext.Msg.alert('Success', "Successfully Added Employee!!");
                                            form.reset();
                                            location.reload();
                                        },
                                        failure: function(form, action) {
                                            Ext.Msg.alert('Failed', action.result.msg);
                                            form.reset();
                                        }
                                    });
                                }
                            }
                        }]
                    })
                ]
            }).show();
        }
    });

    

    // Ext.create('Ext.Button', {
    //     text    : 'Export to Excel',
    //     margin: 20,
    //     renderTo: Ext.getBody(),
    //     handler : function() {
    //         // this button will spit out a different number every time you click it.
    //         // so firstly we must check if that number is already set:
            
    //     }
    // });

    // Ext.create('Ext.Button', {
    //     text    : 'Export to PDF',
    //     margin: 20,
    //     renderTo: Ext.getBody(),
    //     handler : function() {
    //         // this button will spit out a different number every time you click it.
    //         // so firstly we must check if that number is already set:
            
    //     }
    // });
    

    //grid

    Ext.create('Ext.data.Store', {
        storeId: 'simpsonsStore',
        autoLoad: true,
        autoSync: true,
        proxy: {
            type: 'rest',
            actionMethods: {
                read: 'GET'
            },
            url: 'http://localhost:3000/fetch_employees',
            reader: {
                type: 'json',
                rootProperty: 'data'
            },
        }
    });

    var myGrid = Ext.create('Ext.grid.Panel', {
        title: 'Simpsons',
        id: 'enployeeGrid',
        selModel: {
            selType: 'rowmodel', // rowmodel is the default selection model
            mode: 'MULTI' // Allows selection of multiple rows
        },
        enableKeyNav : true,
        stateful: true,
        store: Ext.data.StoreManager.lookup('simpsonsStore'),
        xtype: 'exportbutton',
        columns: [
            {
                text: 'id',
                dataIndex: 'id',
            }, {
                text: 'Name',
                dataIndex: 'name',
                flex: 1,
                editor: {
                    completeOnEnter: false,
    
                    // If the editor config contains a field property, then
                    // the editor config is used to create the Ext.grid.CellEditor
                    // and the field property is used to create the editing input field.
                    field: {
                        xtype: 'textfield',
                        allowBlank: false,
                        listeners : {
                            change : function(field, e) {
                                Ext.Ajax.request({
                                    url: 'http://localhost:3000/employee/'+ Ext.getCmp('enployeeGrid').getSelectionModel().selected.items[0].id, // your backend url
                                    method: 'PUT',
                                    params: {
                                        'id': Ext.getCmp('enployeeGrid').getSelectionModel().selected.items[0].id,
                                        'name': field.getValue()
                                    },
                                });
                            }
                        }
                    }
                }
            }, {
                text: 'Email',
                dataIndex: 'email',
                flex: 1,
                editor: {
                    completeOnEnter: false,
    
                    // If the editor config contains a field property, then
                    // the editor config is used to create the Ext.grid.CellEditor
                    // and the field property is used to create the editing input field.
                    field: {
                        xtype: 'textfield',
                        allowBlank: false,
                        listeners : {
                            change : function(field, e) {
                                Ext.Ajax.request({
                                    url: 'http://localhost:3000/employee/'+ Ext.getCmp('enployeeGrid').getSelectionModel().selected.items[0].id, // your backend url
                                    method: 'PUT',
                                    params: {
                                        'id': Ext.getCmp('enployeeGrid').getSelectionModel().selected.items[0].id,
                                        'email': field.getValue()
                                    },
                                });
                            }
                        }
                    }
                }
            },
            {
                text: 'Phone',
                dataIndex: 'phone',
                flex: 1,
                editor: {
                    completeOnEnter: false,
    
                    // If the editor config contains a field property, then
                    // the editor config is used to create the Ext.grid.CellEditor
                    // and the field property is used to create the editing input field.
                    field: {
                        xtype: 'textfield',
                        allowBlank: false,
                        listeners : {
                            change : function(field, e) {
                                Ext.Ajax.request({
                                    url: 'http://localhost:3000/employee/'+ Ext.getCmp('enployeeGrid').getSelectionModel().selected.items[0].id, // your backend url
                                    method: 'PUT',
                                    params: {
                                        'id': Ext.getCmp('enployeeGrid').getSelectionModel().selected.items[0].id,
                                        'phone': field.getValue()
                                    },
                                });
                            }
                        }
                    }
                }
            },
            {
                text: 'Age',
                dataIndex: 'age',
                flex: 1,
                editor: {
                    completeOnEnter: false,
    
                    // If the editor config contains a field property, then
                    // the editor config is used to create the Ext.grid.CellEditor
                    // and the field property is used to create the editing input field.
                    field: {
                        xtype: 'textfield',
                        allowBlank: false,
                        listeners : {
                            change : function(field, e) {
                                Ext.Ajax.request({
                                    url: 'http://localhost:3000/employee/'+ Ext.getCmp('enployeeGrid').getSelectionModel().selected.items[0].id, // your backend url
                                    method: 'PUT',
                                    params: {
                                        'id': Ext.getCmp('enployeeGrid').getSelectionModel().selected.items[0].id,
                                        'age': field.getValue()
                                    },
                                });
                            }
                        }
                    }
                }
            },
            {
                text: 'Gender',
                dataIndex: 'gender',
                flex: 1,
                editor: {
                    completeOnEnter: true,
    
                    // If the editor config contains a field property, then
                    // the editor config is used to create the Ext.grid.CellEditor
                    // and the field property is used to create the editing input field.
                    field: {
                        xtype: 'textfield',
                        allowBlank: false,
                        listeners : {
                            change : function(field, e) {
                                Ext.Ajax.request({
                                    url: 'http://localhost:3000/employee/'+ Ext.getCmp('enployeeGrid').getSelectionModel().selected.items[0].id, // your backend url
                                    method: 'PUT',
                                    params: {
                                        'id': Ext.getCmp('enployeeGrid').getSelectionModel().selected.items[0].id,
                                        'gender': field.getValue()
                                    },
                                });
                            }
                        }
                    }
                }
            },
        ],
        plugins: {
            ptype: 'cellediting',
            clicksToEdit: 1
        },
        // tools:[{
        //     // type:'refresh',
        //     // tooltip: 'Refresh form Data',
        //     // handler: function(grid){
        //     //     var store = grid.getStore();
        //     //     store.exportFile(grid.columns, 'COOL', 'xlsx');
        //     // }
        //     type:'help',
        //     tooltip: 'Get Help',
        //     callback: function(panel, tool, event) {
        //         // show help here
        //     }
        // }],
        tbar: [
            {
                text: 'XLSX',
                iconCls: 'icon-print',
                handler: function () {
                    let arr = [];
                    myGrid.getStore().each(function(record) {
                        // console.log(JSON.stringify(record.data));
                        arr.push(record.data)
                        let jsonData = JSON.stringify(record.data)
                    });
                    console.log(arr);
                    Ext.Ajax.request({
                        url: 'http://localhost:3000/excel_export', // your backend url
                        method: 'POST',
                        params: {
                            'data': JSON.stringify(arr),
                        },
                    });
                    // Ext.Ajax.request({
                    //     url : 'http://localhost:3000/fetch_employees',
                    //     method:'GET',
                    //     jsonData: storeNew.proxy.reader.rawData,
                    //     scope : this, 
                    //     success : function(response,options) {
                    //         console.log(response)
                    //     },
                    //     //method to call when the request is a failure
                    //     failure: function(response, options){
                    //         alert("FAILURE: " + response.statusText);
                    //     }
                    // });
                }
            },
            {
                text: 'PDF',
                iconCls: 'icon-print',
                handler: function () {
                    let arr = [];
                    myGrid.getStore().each(function(record) {
                        // console.log(JSON.stringify(record.data));
                        arr.push(record.data)
                        let jsonData = JSON.stringify(record.data)
                    });
                    console.log(arr);
                    Ext.Ajax.request({
                        url: 'http://localhost:3000/pdf_export', // your backend url
                        method: 'POST',
                        params: {
                            'data': JSON.stringify(arr),
                        },
                    });
                }
            }
        ],
        height: 500,
        width: "100%",
        renderTo: Ext.getBody()
    });
});