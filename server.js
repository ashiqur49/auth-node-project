//dependencies
const express = require('express');
const inWebRoutes = require('./routes/web');
const connectFlash = require('connect-flash');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const usr = require('./models/index');
const path = require('path');
const bcrypt = require('bcrypt');
const app = express();
app.set('view engine', 'ejs');
app.use(express.static("public"));
app.use(require('body-parser').json())
app.use(require('body-parser').urlencoded({ extended: true }));
app.use(require('express-session')({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: false,
  cookie:{
      secure:false,
      maxAge:100000
  }
}));
app.use(passport.initialize());
app.use(passport.session());

//config session cookie



function validPassword(password){
  bcrypt.compare(password, hash).then(function(result) {
    return result;
  });
}

passport.use(new LocalStrategy(
  async function (username, password, done) {
    await usr.User.findOne({ where: { username } }).then((user) => {
      console.log(user.password);
      if (!user) return done(null, false, { message: `There is no record of the email ${username}.` });
      bcrypt.compare(password, user.password, function(err, res) {
        if (err) return done(err);
        if (res === false) {
          return done(null, false);
        } else {
          return done(null, user);
        }
      });
      // return done(null, false);
     }).catch((err) => {
      console.log(err);
      done(null, false, { message: 'Something went wrong trying to authenticate' });
     })
    }
));

passport.serializeUser(function(user, done) {
    console.log("user"+user);
    done(null, user.id);
});
  
passport.deserializeUser(function(id, done) {
  usr.User.findOne({
   where: {
    'id': id
   }
  }).then(function (user) {
   if (user == null) {
    done(new Error('Wrong user id.'))
   }
   done(null, user)
  })
 })

//enable flash message
// app.use(connectFlash());

//init all webRoute
inWebRoutes(app);
app.post('/login',
  passport.authenticate('local'),
  function(req, res) {
    // If this function gets called, authenticati on was successful.
    // `req.user` contains the authenticated user.
    res.redirect('/');
  });


//init models of db
const db = require('./models');

db.sequelize.sync({force:false}).then((req) => {
    //listening server
    app.listen(3000, ()=>{
        console.log("Server is listening on port 3000");
    });
});




