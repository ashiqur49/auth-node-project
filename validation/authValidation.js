const { body } = require('express-validator');

let validateLogin = [
    body('email', "Invalid Email").isEmail(),
];

module.exports = {
    validateLogin: validateLogin
};