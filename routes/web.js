const express = require('express');
const loginController = require('../controller/loginController');
const schedulerController = require('../controller/schedulerController');
const employeeController = require('../controller/employeeController');
const authController = require('../controller/authController');
const pdfController = require('../controller/pdfController');
let router = express.Router();
const { User } = require('../models/user');
const authValidation = require('../validation/authValidation');
const excel = require('node-excel-export');
var fs = require('fs');

let inWebRoutes = (app) => {
    //autentication middleware
    // router.get('/', authController.authenticate, (req, res) => {
    //     return res.render('index.ejs');
    // });
    router.get('/', (req, res) => {
        return res.render('index.ejs');
    });
    // router.get('/pdf', pdfController.outputPdf);
    router.get('/fetch_employees', employeeController.fetchEmployee);
    router.post('/pdf_export', (req, res) => {
      var pdf = require('html-pdf');
      const jsonParse = JSON.parse(req.body.data);
      var table;
      table += "<table border='1' style='width:100%;word-break:break-word;'>";
      table += "<tr>";
      table += "<th >ID</th>";
      table += "<th >Name</th>";
      table += "<th >Email</th>";
      table += "<th >Phone</th>";
      table += "<th >Age</th>";
      table += "<th >Gender</th>";
      table += "<th >Created</th>";
      table += "<th >Updated</th>";
      table += "</tr>";
      for (var i = 0; i < jsonParse.length; i++) {
          table += "<tr>";
          table += "<td>"+jsonParse[i].id+"</td>";
          table += "<td>"+jsonParse[i].name+"</td>";
          table += "<td>"+jsonParse[i].email+"</td>";
          table += "<td>"+jsonParse[i].phone+"</td>";
          table += "<td>"+jsonParse[i].age+"</td>";
          table += "<td>"+jsonParse[i].gender+"</td>";
          table += "<td>"+jsonParse[i].createdAt+"</td>";
          table += "<td>"+jsonParse[i].updatedAt+"</td>";
          table += "</tr>";
      }
      table += "</table>";
      var options = {
        "format": "A4",
        "orientation": "landscape",
        "border": {
          "top": "0.1in",
      },
      "timeout": "10000"
      };
      pdf.create(table, options).toFile('save_file_path/test.pdf', function(err, result) {
        if (err) return console.log(err);
        console.log("pdf create");
      });
      res.send({
        "success":true, // note this is Boolean, not string
        "msg":"Exported to root directory"
      });
    });
    router.post('/excel_export', (req, res) => {
        // console.log(req.body.data)
        const heading = [
            ['ID', 'Name', 'Phone', 'Email', 'Gender', 'Age', 'Created', 'Updated'] // <-- It can be only values
        ];

        const specification = {
            id: { // <- the key should match the actual data key
              width: 30// <- width in pixels
            },
            name: {
              width: 100 // <- width in chars (when the number is passed as string)
            },
            phone: {
              width: 100 // <- width in pixels
            },
            email: { // <- the key should match the actual data key
              width: 100// <- width in pixels
            },
            gender: {
              width: 50 // <- width in chars (when the number is passed as string)
            },
            age: {
              width: 50 // <- width in pixels
            },
            createdAt: {
              width: 50 // <- width in chars (when the number is passed as string)
            },
            updatedAt: {
              width: 50 // <- width in pixels
            },
        }
        const jsonParse = JSON.parse(req.body.data);
        const report = excel.buildExport(
            [ // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
              {
                name: 'Report', // <- Specify sheet name (optional)
                heading: heading,
                specification: specification, // <- Report specification
                data: jsonParse // <-- Report data
              }
            ]
        );
        res.type("application/octet-stream");
        res.attachment('report.xlsx');
        fs.writeFileSync('report.xlsx', report);
        res.send({
            "success":true, // note this is Boolean, not string
            "msg":"Exported to root directory"
        });
    });
    router.put('/employee/:id', employeeController.updateEmployee);
    router.get('/ext-js', (req, res) => {
        return res.render('ext.ejs');
    });
    router.get('/pdf',authController.authenticate, (req, res) => {
        return res.render('pdf.ejs');
    });
    router.get('/login', loginController.getLoginPage);
    router.get('/node_schedule', schedulerController.scheduleJob);

    router.get('/add_user', (req, res) => {
        User.create({
            name: "Md. Ashiqur Rahman",
            username: "ashiqur49",
            password: "12345"
        }).catch(error =>{
            console.log(error);
        });
        res.send("Data Inserted");
    });
    router.post('/api/add_employee', employeeController.addEmployee);
    return app.use("/", router);
};

module.exports = inWebRoutes;