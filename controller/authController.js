const passport = require('passport');

let authenticate = (req, res, next) => {
  if(!req.user){
    res.redirect("/login");
  } else {
      return next();
  }
};

module.exports = {
    authenticate: authenticate,
}