const { validationResult } = require('express-validator');
const { Employee } = require('../models');

let getEmployeeInfo = (req, res) =>{
    console.log(req.user);
    return res.render('index.ejs');
};

let fetchEmployee = async (req, res) =>{
    const employees = await Employee.findAll()
    const jsonFormat = JSON.stringify(employees, null, 2)
    const jsonParse = JSON.parse(jsonFormat)
    res.send(jsonParse);
};


//inline export
let updateEmployee = async (req, res) => {
    console.log(req.body.name)
    try {
        await Employee.update(req.body, {
            where: {
                id: req.params.id
            }
        });
        res.send({
            "success":true, // note this is Boolean, not string
            "msg":"Employee Updated"
        });
    } catch (err) {
        console.log(err);
    }
}


let addEmployee = (req, res) => {
    if (req.body.name === "" || req.body.email === "" || req.body.phone === "") {
        res.send(
            {
                "employee":null,
                "success":false, // note this is Boolean, not string
                "msg":"Name & Email & Phone Fields Required!"
            }
        );
    }
    else{
        Employee.create({
            name: req.body.name,
            email: req.body.email,
            phone: req.body.phone,
            age: req.body.age,
            gender: req.body.gender,
        }).catch(error =>{
            res.send(
                {
                    "employee":null,
                    "success":false, // note this is Boolean, not string
                    "msg":error
                }
            );
        });
        const employee = {
            name: req.body.name,
            email: req.body.email,
            phone: req.body.phone,
            age: req.body.age,
            gender: req.body.gender,
        }
        res.send(
            {
                "employee":employee,
                "success":true, // note this is Boolean, not string
                "msg":"File uploaded"
            }
        );
    }
}

module.exports = {
    addEmployee: addEmployee,
    getEmployeeInfo: getEmployeeInfo,
    fetchEmployee: fetchEmployee,
    updateEmployee: updateEmployee,
}