const { validationResult } = require('express-validator');


let getLoginPage = (req, res) =>{
    return res.render('login.ejs');
};

module.exports = {
    getLoginPage: getLoginPage
}