const schedule = require('node-schedule');

let scheduleJob = (req, res) =>{
    // const someDate = new Date('2021-10-15T11:33:00.000+6:00');
    // schedule.scheduleJob(someDate, ()=>{
        
    // })
    schedule.scheduleJob('test-job','*/2 * * * * *', ()=>{
        console.log('Job ran in every 2 second');
        //cancel job
        schedule.cancelJob('test-job');
    })
    // return res.render('schedule.ejs', {
    //     data: "job ran @"+new Date().toString()
    // });
};

module.exports = { 
    scheduleJob: scheduleJob
}